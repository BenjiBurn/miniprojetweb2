/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package articles.modeles;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Max
 */
@Entity
public class Article implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String titre;
    private String categorie ;
    @Column(length=2048)
    private String tag;
    @Column(length=2048)
    private String description;
    @Column(length=2048)
    private String screenshot;
    @Column(length=2048)
    private String video;
    @Column(length=2048)
    private String lien;    
    private String date;
    private int note;
    private int nbNote;
    private int totalNote;
    private int nbVue;

    public Article(String titre, String categorie, String tag, String description, String screenshot, String video, String lien) {
        this.titre = titre;
        this.categorie = categorie;
        this.tag = tag;
        this.description = description;
        this.screenshot = screenshot;
        this.video = video;
        this.lien = lien;
        java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat("dd/MM/yy H:mm:ss");
        java.util.Date date = new java.util.Date(); 
        this.date = formater.format(date);
        this.note = 0;
        this.nbNote = 0;
        this.totalNote = 0;
        this.nbVue = 0;
    }

    public Article() {
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getCategorie() {
        return categorie;
    }
    
    public String[] getCategorieSplit() {
        return categorie.split(";");
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getTag() {
        return tag;
    }

    public String[] getTagsSplit(){
        return tag.split(";");
    }
    
    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFirstScreen(){
        String[] screen = screenshot.split(";");
        return screen[0];
    }
    
    public String getScreenshot() {
        return screenshot;
    }
    
    public String[] getScreenshotSplit(){
        return screenshot.split(";");
    }

    public void setScreenshot(String screenshot) {
        this.screenshot = screenshot;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public int getNbNote() {
        return nbNote;
    }
    
    public void incNbNote(){
        this.nbNote++;
    }
    
    public void resNote(){
        setTotalNote((this.note+(this.totalNote*(this.nbNote-1)))/this.nbNote);
    }

    public int getTotalNote() {
        return totalNote;
    }

    private void setTotalNote(int totalNote) {
        this.totalNote = totalNote;
    }

    public int getNbVue() {
        return nbVue;
    }

    public void setNbVue(int nbVue) {
        this.nbVue = nbVue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Article)) {
            return false;
        }
        Article other = (Article) object;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "articles.modeles.Article[ id=" + id + " ]";
    }
    
}
