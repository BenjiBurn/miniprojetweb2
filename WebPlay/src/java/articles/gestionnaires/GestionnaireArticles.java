/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package articles.gestionnaires;

import articles.modeles.Article;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Max
 */
@Stateless
public class GestionnaireArticles {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @PersistenceContext  
    private EntityManager em;
    private int index=0;
    private int maxRes=10;  
    private boolean pass;
    private int nbTotalPageRecherche;

    public Article creeArticle(String titre,String categorie,String tags,String description,String screenshots,String videos,String link) {  
        Article a = new Article(titre,categorie,tags,description,screenshots,videos,link);  
        em.persist(a);  
        return a;
    }    
    
    public void creerArticlesDeTest() {  
        Article a = new Article("Angry Birds ","Jeux","action","Les cochons ont dérobé les oeufs des oiseaux. Ils sont donc en colère !","http://www.jeux-en-html5.fr/img/jeux/angry-birds-img.png","","https://www.angrybirds.com/play");
    
         Article a2 = new Article("Canvas Rider","Jeux","velo;action","Reprise du fameux Line Rider","http://www.jeux-en-html5.fr/img/jeux/canvas-rider-img.png","","http://canvasrider.com/");
         
         Article a3 = new Article("wArp ","Demo","action;tir","Dans ce jeu de plateformes, vous incarnez un petit robot bionique qui doit survivre parmi les différents niveaux du jeu.","http://www.jeux-en-html5.fr/img/jeux/warp-img.png","","http://www.ascendedarcade.com/games/wArp/");

         Article a4 = new Article("Getting started with the AudioContext ","Tutoriels","Musique","Article pour apprendre a utiliser les audio context en html5.","http://2.bp.blogspot.com/-WvF_pSfGh7Y/Tzllg-TPynI/AAAAAAAABCU/AyuApiFdV_o/s1600/HTML5rocks.png","","http://www.html5rocks.com/en/tutorials/webaudio/intro/");
    
           em.persist(a);
           em.persist(a2);
           em.persist(a3);
           em.persist(a4);
    
    }
    
    public Collection<Article> getAllArticle(int indexView) {  
        // Exécution d'une requête équivalente à un select *  
        Query q = em.createQuery("select a from Article a"); 
        q.setMaxResults(maxRes);
        q.setFirstResult(indexView);
        this.index=indexView;         
        this.pass=true;
        return  q.getResultList();
    }

    public void supprimerArticle(String id) {
        Article a = em.find(Article.class, Integer.parseInt(id));
        em.remove(a);
    }
    
    public Article getById(String id) {       
        Article a = em.find(Article.class, Integer.parseInt(id));
        return a;  
    }
    
    public Article updateArticle(String id,String titre,String categorie,String tags,String description,String screenshots,String videos,String link){    
        int val = Integer.parseInt(id);
        Query q = em.createQuery("select a from Article a where a.id=:id");
        q.setParameter("id", val);
        try{
            Article aModif = (Article)q.getSingleResult();
            aModif.setTitre(titre);
            aModif.setCategorie(categorie);
            aModif.setTag(tags);
            aModif.setDescription(description);
            aModif.setScreenshot(screenshots);
            aModif.setVideo(videos);
            aModif.setLien(link);
           return aModif;
        } catch(NoResultException e){
           System.out.println("Pas de résultat - article inexistant ");
           return null;
        }   
    }
    
    public Collection<Article> getArticle(String titre) {  
        Query q = em.createQuery("select a from Article a where UPPER(a.titre) LIKE UPPER('%"+titre+"%')"); 
        this.pass=false;   
        return q.getResultList();  
    }
    
    public Article noter(String id,String note){        
        Article a = em.find(Article.class, Integer.parseInt(id));
        int val = Integer.parseInt(note);
        a.setNote(val);
        a.incNbNote();
        a.resNote();
        return a;
    }
    
    public Collection<Article> searchArticle(String titre,String categorie,int indexView){ 
        Query q;
         if(categorie.equals("all")){ 
            q = em.createQuery("select a from Article a Where UPPER (a.titre) LIKE UPPER ('%"+titre+"%')");
        }else if(!categorie.equals("all")){
            q = em.createQuery("select a from Article a Where UPPER (a.categorie) LIKE UPPER('%"+categorie+"%') and UPPER (a.titre) LIKE UPPER ('%"+titre+"%')");
        }else{
            q = em.createQuery("select a from Article a");
        }
        /*setNbTotalPageRecherche((q.getResultList().size())/maxRes); 
        q.setMaxResults(maxRes);
        q.setFirstResult(indexView);
        this.index=indexView;         
        this.pass=true;*/
        return q.getResultList();     
    }
    
    public Collection<Article> getListTuto(){ 
        Query q = em.createQuery("Select a from Article a where  UPPER (a.categorie) = UPPER ('tutoriels') order by a.date desc");
        q.setMaxResults(5);
        return q.getResultList();     
    }
    
    public Collection<Article> getListJeux(){ 
        Query q = em.createQuery("Select a from Article a where UPPER (a.categorie) = UPPER ('Jeux') order by a.date desc");
        q.setMaxResults(5);
        return q.getResultList();     
    }
    
    public Collection<Article> getListDemo(){ 
        Query q = em.createQuery("Select a from Article a where UPPER (a.categorie) = UPPER ('Demo') order by a.date desc");
        q.setMaxResults(5);
        return q.getResultList();     
    }
    
    public Collection<Article> getListMeilleursNote(){ 
        Query q = em.createQuery("Select a from Article a order by a.totalNote desc");
        q.setMaxResults(5);
        return q.getResultList();     
    }
    
    public Collection<Article> getlisteDerniersAjouts(){ 
        Query q = em.createQuery("Select a from Article a order by a.date desc");
        q.setMaxResults(5);
        return q.getResultList();     
    }

    public Collection<Article> searchByCategorie(String categorie,int indexView) { 
        Query q = em.createQuery("select a from Article a where UPPER(a.categorie) LIKE UPPER('%"+categorie+"%')");   
       /* this.pass=true;
        setNbTotalPageRecherche((q.getResultList().size())/maxRes); 
        q.setMaxResults(maxRes);
        q.setFirstResult(indexView);
        this.index=indexView; */
        return q.getResultList();  
    }

    public Collection<Article> searchByTag(String tags,int indexView) {
        Query q = em.createQuery("select a from Article a where UPPER(a.tag) LIKE UPPER('%"+tags+"%')"); 
        /*this.pass=true;
        setNbTotalPageRecherche((q.getResultList().size())/maxRes); 
        q.setMaxResults(maxRes);
        q.setFirstResult(indexView);
        this.index=indexView;   */
        return q.getResultList();  
    }
    
     public int nbTotalPages(){
        Query q = em.createQuery("select a from Article a order by a.id");
        return ((q.getResultList().size())/maxRes);        
    }
         
    public void listSuivant(){
        this.index=index+1;
    }
    
    public void listPrecedent(){
        this.index=index-1;
    }
    
    public boolean getPass(){
        return this.pass;
    }
    
    public int getIndex(){
        return this.index;
    }
    
    public int getMaxResultat(){
        return this.maxRes;
    }
    
    public void setMaxResultat(int maxRes){
        this.maxRes=maxRes;
    }
    
     public int getNbTotalPageRecherche() {
        return nbTotalPageRecherche;
    }

    public void setNbTotalPageRecherche(int nbTotalPageRecherche) {
        this.nbTotalPageRecherche = nbTotalPageRecherche;
    }
    
}
