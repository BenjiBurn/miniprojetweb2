/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commentaire.gestionnaires;

import articles.modeles.Article;
import commentaires.modeles.Commentaire;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import utilisateurs.modeles.Utilisateur;

/**
 *
 * @author Max
 */
@Stateless
public class GestionnaireCommentaires {
    @PersistenceContext
    private EntityManager em;

    public void ajouterCommentaire(int article, String utilisateur, String commentaire) {
        Commentaire com = new Commentaire(article,utilisateur,commentaire);
        em.persist(com);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    public Collection<Commentaire> getAllCommentaire(int idArticle) {   
        Query q = em.createQuery("select c from Commentaire c where c.idArticle ="+ idArticle ); 
        //q.setParameter("article", idArticle); 
        return  q.getResultList();
    }
    
}
