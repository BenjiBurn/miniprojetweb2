/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commentaires.modeles;

import articles.modeles.Article;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import utilisateurs.modeles.Utilisateur;

/**
 *
 * @author Max
 */
@Entity
public class Commentaire implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int idArticle;
    private String pseudo;
    @Column(length=2048)
    private String commentaire;    
    private String date;
    
    public Commentaire (){}
    
    public Commentaire(int idArticle,String pseudo,String commentaire){
        this.idArticle = idArticle;
        this.pseudo = pseudo;
        this.commentaire = commentaire;        
        java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat("dd/MM/yy H:mm:ss");
        java.util.Date date = new java.util.Date(); 
        this.date = formater.format(date);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }    
    
    
    public int getArticle() {
        return idArticle;
    }

    public void setArticle(int article) {
        this.idArticle = article;
    }

    public String getUtilisateur() {
        return this.pseudo;
    }

    public void setUtilisateur(String idUtilisateur) {
        this.pseudo = idUtilisateur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }    

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commentaire)) {
            return false;
        }
        Commentaire other = (Commentaire) object;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "commentaires.modeles.Commentaire[ id=" + id + " ]";
    }
    
}
