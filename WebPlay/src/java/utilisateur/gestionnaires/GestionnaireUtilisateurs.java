/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilisateur.gestionnaires;

import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import utilisateurs.modeles.Utilisateur;

/**
 *
 * @author Max
 */
@Stateless
public class GestionnaireUtilisateurs {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    @PersistenceContext  
    private EntityManager em;  
    private int index=0;
    private int maxRes=10;
    private boolean pass = true;
    
    public Utilisateur creeUtilisateur(String nom,String prenom,String login,String mdp,String email) {  
        Utilisateur u = new Utilisateur(nom,prenom,login,mdp,email);  
        em.persist(u);  
        return u;  
    }  
    
    public void creerAdmin(){
        Utilisateur admin = new Utilisateur("admin","admin","admin","admin01","admin@gmail.com");
        admin.setAdmin(true);
        em.persist(admin);
    }
    
    public Collection<Utilisateur> getAllUsers(int indexView) {  
        Query q = em.createQuery("select u from Utilisateur u");
        this.pass=true;
        q.setMaxResults(maxRes);
        q.setFirstResult(indexView);
        this.index=indexView;  
        return  q.getResultList();
    }  
    
    public void supprimerUtilisateur(String id) {
        Utilisateur u = em.find(Utilisateur.class, Integer.parseInt(id));
        em.remove(u);
    }
    
    public Utilisateur getById(String id) {       
        Utilisateur u = em.find(Utilisateur.class, Integer.parseInt(id));
        this.pass=false;
        return u;  
    }
    
    public Utilisateur updateUtilisateur(String id,String nom,String prenom,String login,String email,boolean isAdmin){    
        int val = Integer.parseInt(id);
        Query q = em.createQuery("select u from Utilisateur u where u.id=:id");
        q.setParameter("id", val);
        System.out.println(isAdmin);
        try{
            Utilisateur uModif = (Utilisateur)q.getSingleResult();
            uModif.setNom(nom);
            uModif.setPrenom(prenom);
            uModif.setLogin(login);
            uModif.setEmail(email);     
            uModif.setAdmin(isAdmin);
           return uModif;
        } catch(NoResultException e){
           System.out.println("Pas de résultat - utilisateur inexistant ");
           return null;
        }   
    }
    
    public Utilisateur getUser(String login) {  
        Query q = em.createQuery("select u from Utilisateur u where u.login=:login");  
        q.setParameter("login", login);
        Utilisateur user = (Utilisateur) q.getSingleResult();
        return user;  
    }   
    
    public boolean checkUserPassword(String login,String mdp){                
        Query q = em.createQuery("select u from Utilisateur u where u.login=:login");  
        q.setParameter("login", login);
        try{ 
            Utilisateur user = (Utilisateur) q.getSingleResult();
            if (login.equalsIgnoreCase(user.getLogin())&&mdp.equals(user.getMdp())) { 
               return true;
            }  else{
                return false;
            }
        }catch(Exception e){ 	          
           return false; 
        } 
    }
    
    public int nbTotalPages(){
        Query q = em.createQuery("select u from Utilisateur u order by u.id");
        return ((q.getResultList().size())/maxRes);        
    }
    
    public void listSuivant(){
        this.index=index+1;
    }
    
    public void listPrecedent(){
        this.index=index-1;
    }
    
    public boolean getPass(){
        return this.pass;
    }
    
    public int getIndex(){
        return this.index;
    }
    
    public int getMaxResultat(){
        return this.maxRes;
    }
    
    public void setMaxResultat(int maxRes){
        this.maxRes=maxRes;
    }
}
