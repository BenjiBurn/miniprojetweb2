/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import articles.gestionnaires.GestionnaireArticles;
import articles.modeles.Article;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utilisateur.gestionnaires.GestionnaireUtilisateurs;
import utilisateurs.modeles.Utilisateur;

/**
 *
 * @author Max
 */
@WebServlet(name = "ServletOnLoad", urlPatterns = {"/ServletOnLoad"})
public class ServletOnLoad extends HttpServlet {
    @EJB
    private GestionnaireUtilisateurs gestionnaireUtilisateurs;
    @EJB
    private GestionnaireArticles gestionnaireArticles;
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            String action = request.getParameter("action");            
            String forwardTo = "";  
            String message = ""; 
           
            if (action.equals("listOnLoad")) { 
                if(gestionnaireUtilisateurs.getAllUsers(0).isEmpty()){
                    gestionnaireUtilisateurs.creerAdmin();
                }
                if(gestionnaireArticles.getAllArticle(0).isEmpty()){
                    gestionnaireArticles.creerArticlesDeTest();
                }
                Collection<Article> listeMeilleursNote = gestionnaireArticles.getListMeilleursNote();  
                request.setAttribute("listeMeilleurNote", listeMeilleursNote); 
                Collection<Article> listeDerniersAjouts = gestionnaireArticles.getlisteDerniersAjouts();  
                request.setAttribute("listeDerniersAjouts", listeDerniersAjouts); 
                Collection<Article> listeJeux = gestionnaireArticles.getListJeux();  
                request.setAttribute("listeJeux", listeJeux);  
                Collection<Article> listeTuto = gestionnaireArticles.getListTuto();  
                request.setAttribute("listeTuto", listeTuto);  
                Collection<Article> listeDemo = gestionnaireArticles.getListDemo();  
                request.setAttribute("listeDemo", listeDemo); 
                forwardTo = "index.jsp?action=onload";  
                message = "loadPage";  
            }
                           
            else {
                forwardTo = "ServletOnLoad?action=listOnLoad";  
                message = "La fonctionnalité pour le paramètre " + action + " est à implémenter !";  
            } 
        RequestDispatcher dp = request.getRequestDispatcher(forwardTo + "&message=" + message /*+ "&connexion="+boolConnec*/);  
        dp.forward(request, response); 
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
