/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import articles.gestionnaires.GestionnaireArticles;
import articles.modeles.Article;
import commentaire.gestionnaires.GestionnaireCommentaires;
import commentaires.modeles.Commentaire;
import java.io.IOException;
import java.util.Collection;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utilisateur.gestionnaires.GestionnaireUtilisateurs;
import utilisateurs.modeles.Utilisateur;

/**
 *
 * @author Max
 */
@WebServlet(name = "ServletCommentaire", urlPatterns = {"/ServletCommentaire"})
public class ServletCommentaire extends HttpServlet {
    @EJB
    private GestionnaireUtilisateurs gestionnaireUtilisateurs;    
    @EJB
    private GestionnaireArticles gestionnaireArticles;
    @EJB
    private GestionnaireCommentaires gestionnaireCommentaires;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
            
        String action = request.getParameter("action"); 
        String idArticle = request.getParameter("article"); 
        String pseudo = request.getParameter("utilisateur"); 
        String commentaire = request.getParameter("commentaire"); 
        String forwardTo = "";  
        String message = "";  
        
        if (action != null) {
            if(action.equals("newCommentaire")){                   
                Article article = gestionnaireArticles.getById(idArticle);
                gestionnaireCommentaires.ajouterCommentaire(Integer.parseInt(idArticle),pseudo,commentaire);
                Collection<Commentaire> listComment = gestionnaireCommentaires.getAllCommentaire(Integer.parseInt(idArticle));
                request.setAttribute("listeCommentaire", listComment); 
                request.setAttribute("article", article); 
                forwardTo = "article.jsp?action=todo";  
                message = "Afficher un article"; 
            
            }
              else if(action.equals("afficherCommentaire")){
                    Article article = gestionnaireArticles.getById(idArticle);                    
                    Collection<Commentaire> listComment = gestionnaireCommentaires.getAllCommentaire(Integer.parseInt(idArticle));
                    request.setAttribute("listeCommentaire", listComment); 
                    request.setAttribute("article", article);
                    forwardTo = "article.jsp?action=todo";  
                    message = "Afficher un article"; 
            }
            
              else {
                forwardTo = "ServletOnLoad?action=listOnLoad";    
                message = "La fonctionnalité pour le paramètre " + action + " est à implémenter !";  
            }
        }  
        RequestDispatcher dp = request.getRequestDispatcher(forwardTo + "&message=" + message /*+ "&connexion="+boolConnec*/);  
        dp.forward(request, response); 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
