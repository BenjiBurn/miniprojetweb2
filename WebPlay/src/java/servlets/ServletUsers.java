/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utilisateur.gestionnaires.GestionnaireUtilisateurs;
import utilisateurs.modeles.Utilisateur;

/**
 *
 * @author Max
 */
@WebServlet(name = "ServletUsers", urlPatterns = {"/ServletUsers"})
public class ServletUsers extends HttpServlet {
    @EJB
    private GestionnaireUtilisateurs gestionnaireUtilisateurs;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");  
        String nom = request.getParameter("nom");  
        String prenom = request.getParameter("prenom"); 
        String login = request.getParameter("login");  
        String mdp = request.getParameter("mdp");
        String email = request.getParameter("email");
        boolean isAdmin = Boolean.parseBoolean( request.getParameter("checkAdmin") );
        String forwardTo = "";  
        String message = "";  
        String id = request.getParameter("id");
        String nb = request.getParameter("nb");
        String select[] = request.getParameterValues("checkSupp");
        String indexValue = request.getParameter("index");
          
        if (action != null) {
            if (action.equals("newUser")){
                Utilisateur newU = gestionnaireUtilisateurs.creeUtilisateur(nom,prenom,login,mdp,email);
                Collection<Utilisateur> liste = gestionnaireUtilisateurs.getAllUsers(0);
                request.setAttribute("listeUsers", liste);  
                forwardTo = "ServletOnLoad?action=listOnLoad";  
                message = "Ajout d'un utilisateur"; 
            }
            
            else if (action.equals("listeUsers")) {  
                Collection<Utilisateur> liste = gestionnaireUtilisateurs.getAllUsers(0);  
                request.setAttribute("listeUsers", liste);  
                forwardTo = "admin.jsp?action=listeUsers";  
                message = "Liste des utilisateurs";  
            }
            
            else if (action.equals("updateUtilisateur")){ 
                gestionnaireUtilisateurs.updateUtilisateur(id,nom,prenom,login,email,isAdmin); 
                Collection<Utilisateur> liste = gestionnaireUtilisateurs.getAllUsers(0);
                request.setAttribute("listeUsers", liste);
                forwardTo = "admin.jsp?action=listeUsers";  
                message = "Modification d'un utilisateur"; 
           } 
            
            else if (action.equals("supprimerUtilisateur")){ 
               gestionnaireUtilisateurs.supprimerUtilisateur(id);  
               Collection<Utilisateur> liste = gestionnaireUtilisateurs.getAllUsers(0);  
               request.setAttribute("listeUsers", liste);  
               forwardTo ="admin.jsp?action=listeUsers";  
               message = "Suppression d'un utilisateur";             
            }
            
            else if (action.equals("supprimerPlusieursUtilisateurs")) { 
                    if (select != null && select.length != 0) {                        
                        for (int i = 0; i < select.length; i++) {
                            gestionnaireUtilisateurs.supprimerUtilisateur(select[i]); 
                        }
                    } 
                    Collection<Utilisateur> liste = gestionnaireUtilisateurs.getAllUsers(0);   
                    request.setAttribute("listeUsers", liste);  
                    forwardTo = "admin.jsp?action=listeUsers";  
                    message = "Suppression multiple d'utilisateur"; 
            }
            
             else if (action.equals("modifierUtilisateur")){  
                Utilisateur modif = gestionnaireUtilisateurs.getById(id);
                request.setAttribute("user", modif);
                forwardTo = "admin.jsp?action=modifierUtilisateur";  
                message = "Modification d'un utilisateur"; 
            }
             
            else if (action.equals("chercherParLogin")){
                List<Utilisateur> list = new ArrayList<>();
                Utilisateur user = gestionnaireUtilisateurs.getUser(login); 
                list.add(user);
                request.setAttribute("listeUsers", list);  
                forwardTo = "admin.jsp?action=listeUsers";  
                message = "Recherche d'un utilisateur"; 
            }
            
            else if (action.equals("suivant")){
                    gestionnaireUtilisateurs.listSuivant();
                    Collection<Utilisateur> liste = gestionnaireUtilisateurs.getAllUsers(gestionnaireUtilisateurs.getIndex());
                    request.setAttribute("listeUtilisateurs", liste); 
                    forwardTo = "admin.jsp?action=listeUtilisateur";  
                    message = "Liste des utilisateurs";
            }
            
             else if (action.equals("precedent")){
                    gestionnaireUtilisateurs.listPrecedent();
                    Collection<Utilisateur> liste = gestionnaireUtilisateurs.getAllUsers(gestionnaireUtilisateurs.getIndex());  
                    request.setAttribute("listeUtilisateurs", liste); 
                    forwardTo = "admin.jsp?action=listeUtilisateur";  
                    message = "Liste des utilisateurs";
            }
            
             else if (action.equals("getPage")){
                Collection<Utilisateur> liste = gestionnaireUtilisateurs.getAllUsers(Integer.parseInt(indexValue)-1);  
                request.setAttribute("listeUtilisateurs", liste);  
                forwardTo = "admin.jsp?action=listeUtilisateur";  
                message = "Liste des utilisateurs";
                           
            }
            
            else {
                forwardTo = "index.jsp?action=todo";  
                message = "La fonctionnalité pour le paramètre " + action + " est à implémenter !";  
            }  
                request.setAttribute("pageNumber", gestionnaireUtilisateurs.getIndex()+1);
                request.setAttribute("nbTotalPages", gestionnaireUtilisateurs.nbTotalPages());
        } 
        
        RequestDispatcher dp = request.getRequestDispatcher(forwardTo + "&message=" + message);  
        dp.forward(request, response); 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
