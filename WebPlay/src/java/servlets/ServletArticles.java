/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import articles.gestionnaires.GestionnaireArticles;
import articles.modeles.Article;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Max
 */
@WebServlet(name = "ServletArticles", urlPatterns = {"/ServletArticles"})
public class ServletArticles extends HttpServlet {
    @EJB
    private GestionnaireArticles gestionnaireArticles;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String action = request.getParameter("action"); 
        String id = request.getParameter("id");
        String titre = request.getParameter("titre");  
        String categorie = request.getParameter("categories");  
        String tags = request.getParameter("tags"); 
        String description = request.getParameter("description");
        String screenshots = request.getParameter("screenshots");
        String videos = request.getParameter("videos");
        String link = request.getParameter("link");        
        String note = request.getParameter("note");
        String select[] = request.getParameterValues("checkSupp");
        String indexValue = request.getParameter("index");
        
        String forwardTo = "";  
        String message = "";  
        
        if (action != null) {
            if(action.equals("newArticle")){
                Article a = gestionnaireArticles.creeArticle(titre, categorie, tags, description, screenshots, videos, link);
                Collection<Article> list = gestionnaireArticles.getAllArticle(0);
                request.setAttribute("listeArticles", list);
                forwardTo = "admin.jsp?action=listeArticle";  
                message = "Ajout d'un Article";         
            }
            
            else if(action.equals("listeArticle")){
                Collection<Article> list = gestionnaireArticles.getAllArticle(0);
                request.setAttribute("listeArticles", list);
                forwardTo = "admin.jsp?action=listeArticle";  
                message = "Ajout d'un Article"; 
            }
            
            else if (action.equals("updateArticle")){ 
                gestionnaireArticles.updateArticle(id,titre,categorie,tags,description,screenshots,videos,link); 
                Collection<Article> liste = gestionnaireArticles.getAllArticle(0);
                request.setAttribute("listeArticles", liste);
                forwardTo = "admin.jsp?action=listeArticle";  
                message = "Modification d'un article"; 
           } 
            
            else if (action.equals("supprimerArticle")){ 
               gestionnaireArticles.supprimerArticle(id);  
               Collection<Article> liste = gestionnaireArticles.getAllArticle(0);  
               request.setAttribute("listeArticles", liste);  
               forwardTo ="admin.jsp?action=listeArticle";  
               message = "Suppression d'un article";             
            }
            
            else if (action.equals("supprimerPlusieursArticles")) { 
                    if (select != null && select.length != 0) {                        
                        for (int i = 0; i < select.length; i++) {
                            gestionnaireArticles.supprimerArticle(select[i]); 
                        }
                    } 
                    Collection<Article> liste = gestionnaireArticles.getAllArticle(0);   
                    request.setAttribute("listeArticles", liste);  
                    forwardTo = "admin.jsp?action=listeArticle";  
                    message = "Suppression de tous les Articles"; 
            }
            
             else if (action.equals("modifierArticle")){  
                Article modif = gestionnaireArticles.getById(id);
                request.setAttribute("article", modif);
                forwardTo = "admin.jsp?action=updateArticle";  
                message = "Modification d'un utilisateur"; 
            }                        
             
             else if (action.equals("chercherParTitre")){
                Collection<Article> liste = gestionnaireArticles.getArticle(titre); 
                request.setAttribute("listeArticles", liste);  
                forwardTo = "admin.jsp?action=listeArticle";  
                message = "Recherche d'un article"; 
            }
             
             else if (action.equals("consulterArticle")){  
                Article consulter = gestionnaireArticles.getById(id);
                request.setAttribute("article", consulter);
                forwardTo = "ServletCommentaire?action=afficherCommentaire&article="+id; 
                message = "Afficher un article"; 
            }
             
             else if (action.equals("noterArticle")){  
                Article noter = gestionnaireArticles.noter(id,note);
                Article consulter = gestionnaireArticles.getById(id);
                request.setAttribute("article", consulter);
                forwardTo = "ServletCommentaire?action=afficherCommentaire&article="+id; 
                message = "Afficher un article"; 
            }                        
             
              else if (action.equals("creerArticleDeTest")) {  
                gestionnaireArticles.creerArticlesDeTest();  
                Collection<Article> liste = gestionnaireArticles.getAllArticle(0);  
                request.setAttribute("listeArticles", liste);  
                forwardTo = "admin.jsp?action=listeArticle";  
                message = "Liste des utilisateurs";  
            }
             
              else if (action.equals("listOnLoad")) {                     
                Collection<Article> listeMeilleursNote = gestionnaireArticles.getListMeilleursNote();  
                request.setAttribute("listeMeilleurNote", listeMeilleursNote); 
                Collection<Article> listeDerniersAjouts = gestionnaireArticles.getlisteDerniersAjouts();  
                request.setAttribute("listeDerniersAjouts", listeDerniersAjouts); 
                Collection<Article> listeJeux = gestionnaireArticles.getListJeux();  
                request.setAttribute("listeJeux", listeJeux);  
                Collection<Article> listeTuto = gestionnaireArticles.getListTuto();  
                request.setAttribute("listeTuto", listeTuto);  
                Collection<Article> listeDemo = gestionnaireArticles.getListDemo();  
                request.setAttribute("listeDemo", listeDemo); 
                forwardTo = "index.jsp?action=onload";  
                message = "loadPage";  
            }
               
              else if (action.equals("suivant")){
                    gestionnaireArticles.listSuivant();
                    Collection<Article> liste = gestionnaireArticles.getAllArticle(gestionnaireArticles.getIndex());
                    request.setAttribute("listeArticles", liste); 
                    forwardTo = "admin.jsp?action=listeArticle";  
                    message = "Liste des utilisateurs";
            }
            
             else if (action.equals("precedent")){
                    gestionnaireArticles.listPrecedent();
                    Collection<Article> liste = gestionnaireArticles.getAllArticle(gestionnaireArticles.getIndex());  
                    request.setAttribute("listeArticles", liste); 
                    forwardTo = "admin.jsp?action=listeArticle";  
                    message = "Liste des utilisateurs";
            }
            
             else if (action.equals("getPage")){
                Collection<Article> liste = gestionnaireArticles.getAllArticle(Integer.parseInt(indexValue)-1);  
                request.setAttribute("listeArticles", liste);  
                forwardTo = "admin.jsp?action=listeArticle";  
                message = "Liste des utilisateurs";
                           
            }
            
             else {
                forwardTo = "ServletArticles?action=listOnLoad";    
                message = "La fonctionnalité pour le paramètre " + action + " est à implémenter !";  
            }              
                request.setAttribute("pageNumber", gestionnaireArticles.getIndex()+1);
                request.setAttribute("nbTotalPages", gestionnaireArticles.nbTotalPages());
        }
        RequestDispatcher dp = request.getRequestDispatcher(forwardTo + "&message=" + message /*+ "&connexion="+boolConnec*/);  
        dp.forward(request, response); 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
