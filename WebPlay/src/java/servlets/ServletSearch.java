/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import articles.gestionnaires.GestionnaireArticles;
import articles.modeles.Article;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Max
 */
@WebServlet(name = "ServletSearch", urlPatterns = {"/ServletSearch"})
public class ServletSearch extends HttpServlet {
    @EJB
    private GestionnaireArticles gestionnaireArticles;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String indexValue = request.getParameter("index");
        String titre = request.getParameter("titre");  
        String categorie = request.getParameter("categories");  
        String tags = request.getParameter("tags"); 
        String typeRecherche="";
        String forwardTo = "";  
        String message = "";  
            
        if (action != null) {
            
            if(action.equals("searchArticle")){  
                typeRecherche=categorie;
                Collection<Article> articleSearch = gestionnaireArticles.searchArticle(titre,categorie,0);
                if(articleSearch.isEmpty()){
                    forwardTo = "search.jsp?action=empty";
                }else{ 
                    request.setAttribute("listeArticles", articleSearch);
                    forwardTo = "search.jsp?action=res";                 
                    
                }
                message = "Afficher recherche"; 
             }
             
             else if(action.equals("searchByCategorie")){                    
                typeRecherche = "categorie";
                Collection<Article> articleSearch = gestionnaireArticles.searchByCategorie(categorie,0);
                if(articleSearch.isEmpty()){
                    forwardTo = "search.jsp?action=empty";
                }else{ 
                    request.setAttribute("listeArticles", articleSearch);
                    forwardTo = "search.jsp?action=res";                 
                    
                }
                message = "Afficher recherche"; 
             }
             
             else if(action.equals("searchByTag")){  
                typeRecherche = "tag";
                Collection<Article> articleSearch = gestionnaireArticles.searchByTag(tags,0);
                if(articleSearch.isEmpty()){
                    forwardTo = "search.jsp?action=empty";
                }else{ 
                    request.setAttribute("listeArticles", articleSearch);
                    forwardTo = "search.jsp?action=res";                 
                    
                }
                message = "Afficher recherche"; 
             }
            
                /*else if (action.equals("suivant")){
                gestionnaireArticles.listSuivant();
                Collection<Article> articleSearch = null;
                if(typeRecherche.equals("tag")){
                    articleSearch = gestionnaireArticles.searchByTag(tags,gestionnaireArticles.getIndex());
                }else if(typeRecherche.equals("categorie")){
                    articleSearch = gestionnaireArticles.searchByCategorie(categorie,gestionnaireArticles.getIndex());
                }else{
                    articleSearch = gestionnaireArticles.getAllArticle(gestionnaireArticles.getIndex());  
                }
                request.setAttribute("listeArticles", articleSearch); 
                forwardTo = "search.jsp?action=res";  
                message = "Liste des utilisateurs";
            }
            
             else if (action.equals("precedent")){
                    gestionnaireArticles.listPrecedent();
                    Collection<Article> articleSearch = null;
                    if(typeRecherche.equals("tag")){
                        articleSearch = gestionnaireArticles.searchByTag(tags,gestionnaireArticles.getIndex());
                    }else if(typeRecherche.equals("categorie")){
                        articleSearch = gestionnaireArticles.searchByCategorie(categorie,gestionnaireArticles.getIndex());
                    }else{
                        articleSearch = gestionnaireArticles.getAllArticle(gestionnaireArticles.getIndex());  
                    }
                    request.setAttribute("listeArticles", articleSearch); 
                    forwardTo = "search.jsp?action=res";  
                    message = "Liste des utilisateurs";
            }
            
             else if (action.equals("getPage")){
                Collection<Article> articleSearch = null;
                if(typeRecherche.equals("tag")){
                    articleSearch = gestionnaireArticles.searchByTag(tags,Integer.parseInt(indexValue)-1);
                }else if(typeRecherche.equals("categorie")){
                    articleSearch = gestionnaireArticles.searchByCategorie(categorie,Integer.parseInt(indexValue)-1);
                }else{
                    articleSearch = gestionnaireArticles.getAllArticle(Integer.parseInt(indexValue)-1);
                }
                request.setAttribute("listeArticles", articleSearch);  
                forwardTo = "search.jsp?action=res";  
                message = "Liste des utilisateurs";
                           
            }*/
            
             else {
                forwardTo = "ServletArticles?action=listOnLoad";    
                message = "La fonctionnalité pour le paramètre " + action + " est à implémenter !";  
            }   
                request.setAttribute("pageNumber", gestionnaireArticles.getIndex()+1);
                request.setAttribute("nbTotalPages", gestionnaireArticles.getNbTotalPageRecherche());
        }
        RequestDispatcher dp = request.getRequestDispatcher(forwardTo + "&message=" + message /*+ "&connexion="+boolConnec*/);  
        dp.forward(request, response);
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
