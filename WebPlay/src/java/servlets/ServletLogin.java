/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utilisateur.gestionnaires.GestionnaireUtilisateurs;
import utilisateurs.modeles.Utilisateur;

/**
 *
 * @author Max
 */
@WebServlet(name = "ServletLogin", urlPatterns = {"/ServletLogin"})
public class ServletLogin extends HttpServlet {
    @EJB
    private GestionnaireUtilisateurs gestionnaireUtilisateurs;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String login = request.getParameter("pseudo");
        String pass = request.getParameter("password");
        String forwardTo = "";  
        String message = "";  
        
        // Si tous les champs du formulaire ont été remplis        
        if (action != null) {                 
            HttpSession session = request.getSession(true);
            if (action.equals("login")) {
                //Si le service valide l'utilisateur
                    if (gestionnaireUtilisateurs.checkUserPassword(login, pass)) { 
                        Utilisateur compte = gestionnaireUtilisateurs.getUser(login);
                        if(compte.isAdmin()){                            
                            session.setAttribute("ADMIN", login);
                        }else{
                            session.setAttribute("USER", login);
                        }
                        forwardTo = "ServletOnLoad?action=listOnLoad";
                        message = "Connexion réussie";
                    }else{
                        forwardTo = "ServletOnLoad?action=listOnLoad";
                        message = "Connexion échouée";
                    }
            }else if (action.equals("logout")) {                     
                    session.invalidate();
                    forwardTo = "ServletOnLoad?action=listOnLoad";
                    message = "Déconnexion réussie";
                
            }else{

                forwardTo = "ServletOnLoad?action=listOnLoad";  
                message = "La fonctionnalité pour le paramètre " + action + " est à implémenter !"; 
            }
        }
        // Si le formulaire est incomplet, renvoi vers le formulaire avec un message d'erreur
        RequestDispatcher dp = request.getRequestDispatcher(forwardTo + "&message=" + message /*+ "&connexion="+boolConnec*/);  
        dp.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
