<h2 class="titreadmin">Menu d'admin</h2>
<c:if test="${empty param['action']}"> 
    <br />
    <h4 class="titreadmin">Selectionnez une action</h4>  
</c:if> 

<c:if test="${param['action'] == 'newArticle'}" >
    <div class="panel panel-default">
        <div class="panel-heading" style="text-align: center;font-size: 20px;">Nouvel article</div>
        <div class="panel-body">
            <form action="ServletArticles" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="titre">Titre</label>
                            <input type="text" class="form-control" id="titre" name="titre" placeholder="Nom du jeu">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="categories">Cat�gorie</label>
                            <input type="text" class="form-control" id="categories" name="categories" placeholder="Cat�gorie">
                        </div>
                    </div>
                </div>
                <!--<div class="form-group">
                    <form>
                        <label for="tags">Tags</label>
                        <div id="tag-info" class="input-append">
                            <input type="text" class="tags" placeholder="Entrez les tags">
                            <button class="btn btn-primary btn-sm" type="button">Add</button>
                        </div>
                        <ul id="tag-cloud"></ul>
                    </form>
                </div>-->
                <div class="form-group">
                    <label for="tags">Tags</label>
                    <input type="text" class="form-control" id="tags" name="tags" placeholder="Tags s�par�s par des points-virgules">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" rows="3" placeholder="Description de l'article"></textarea>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="screenshots">Screenshots</label>
                            <textarea class="form-control" id="screenshots" name="screenshots" rows="3" placeholder="Liens s�par�s par des points-virgules"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="videos">Vid�os</label>
                            <textarea class="form-control" id="videos" name="videos" rows="3" placeholder="Liens s�par�s par des points-virgules"></textarea>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="link">Lien</label>
                    <input type="text" class="form-control" id="link" name="link" placeholder="Lien vers le jeu">
                    <input type="hidden" name="action" value="newArticle"/>
                </div>


                <center><input type="submit" value="Envoyer" name="submit" class="btn btn-primary"/></center>
            </form>
        </div>
    </div>
</c:if>
    
<c:if test="${param['action'] == 'listeArticle'}" >
    <div class="panel panel-default">
        <div class="panel-heading" style="text-align: center;font-size: 20px;">Liste des articles</div>
        <div class="panel-body">
            <table class="table table-hover" id='cases'>
                <!-- La ligne de titre du tableau des comptes -->
                <thead>
                    <tr>
                        <th><input type='checkbox' id='cocheTout'/></th>
                        <th>Titre</th>
                        <th>Cat�gorie</th>
                        <th>Note</th>
                        <th>Modifier</th>
                        <th>Supprimer</th>                    
                    </tr>
                </thead>
                <tbody>
                    <!-- Ici on affiche les lignes, une par utilisateur -->
                    <!-- cette variable montre comment on peut utiliser JSTL et EL pour calculer -->
                <c:set var="total" value="0"/>
                <form action="ServletArticles" method="post">
                    <c:forEach var="a" items="${listeArticles}">
                        <tr id='cases'>
                            <td><input type="checkbox" name="checkSupp" value="${a.id}"</td>
                            <td><a href="ServletArticles?action=consulterArticle&id=${a.id}">${a.titre}</td>
                            <td>${a.categorie}</td>
                            <td>${a.totalNote}</td>
                            <td><a href="ServletArticles?action=modifierArticle&id=${a.id}"><img width="20px" src="ressources/crayon.png"/></a></td>
                            <td><a href="ServletArticles?action=supprimerArticle&id=${a.id}"><img src="ressources/supprimer.png"/></a></td>
                            <!-- On compte le nombre de users -->
                        <c:set var="total" value="${total+1}"/>
                        </tr>
                    </c:forEach>

                    <!-- Affichage du solde total dans la derni�re ligne du tableau -->
                    <tr>                       
                        <td></td><td><b>TOTAL</b></td><td></td><td><b>${total}</b></td>
                        <td>                            
                        </td>
                        <td>                        
                            <input type="hidden" name="action" value="supprimerPlusieursArticles"/>
                            <input type="submit" class="btn btn-primary" value="Delete All Selected" name="submit"/>
                        </td>
                    </tr> 
                    </tbody>
            </table>
        </div>
        <center><nav>
            <ul class="pagination">
            <c:if test="${requestScope['pageNumber'] gt 1}">
                <li>
                <a href="ServletArticles?action=precedent&index=${requestScope['pageNumber'] - 1}" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
            </c:if>

            <c:forEach begin="1" end="${requestScope['nbTotalPages'] }" var="i">
                <c:choose>
                    <c:when test="${i!=pageNumber}">
                        <li><a class="nav" href="ServletArticles?action=getPage&index=${i}">${i} </a></li>
                    </c:when>
                    <c:otherwise>
                        <li class="active"><a class="nav" href="ServletArticles?action=getPage&index=${i}">${i} </a></li>
                    </c:otherwise>        
                </c:choose>       
            </c:forEach>

            <c:if test="${requestScope['pageNumber'] lt requestScope['nbTotalPages'] }">
                <li>
                    <a href="ServletArticles?action=suivant&index=${requestScope['pageNumber'] + 1}" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
            </c:if>
            </ul>
        </nav></center>
</c:if>

<c:if test="${param['action'] == 'updateArticle'}" >
    <div class="panel panel-default">
        <div class="panel-heading" style="text-align: center;font-size: 20px;">Modification de l'article</div>
        <div class="panel-body">
            <form action="ServletArticles" method="post" >
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="titre">Titre</label>
                            <input type="text" class="form-control" id="titre" name="titre" value="${article.getTitre()}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="categories">Cat�gorie</label>
                            <input type="text" class="form-control" id="categories" name="categories" value="${article.getCategorie()}">
                        </div>
                    </div>
                </div>
                <!--<div class="form-group">
                    <form>
                        <label for="tags">Tags</label>
                        <div id="tag-info" class="input-append">
                            <input type="text" class="tags" placeholder="Entrez les tags">
                            <button class="btn btn-primary btn-sm" type="button">Add</button>
                        </div>
                        <ul id="tag-cloud"></ul>
                    </form>
                </div>-->
                <div class="form-group">
                    <label for="tags">Tags</label>
                    <input type="text" class="form-control" id="tags" name="tags" value="${article.getTag()}">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" rows="3" >${article.getDescription()}</textarea>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="screenshots">Screenshots</label>
                            <textarea class="form-control" id="screenshots" name="screenshots" rows="3">${article.getScreenshot()}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="videos">Vid�os</label>
                            <textarea class="form-control" id="videos" name="videos" rows="3">${article.getVideo()}</textarea>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="link">Lien</label>
                    <textarea type="text" class="form-control" id="link" name="link">${article.getLien()}</textarea>
                </div>


                <center>
                            <input type="hidden" name="action" value="updateArticle"/>
                            <input type="hidden" name="id" value="${article.getId()}"/>
                            <input type="submit" class="btn btn-primary" value="Mettre � jour" name="submit"/>
                </center>
            </form>
        </div>
    </div>
</c:if>
                            
<c:if test="${param['action'] == 'listeUsers'}" >
    <h2 class="titre2">Liste des utilisateurs</h2>
    <table class="table table-hover" id='cases'>
        <!-- La ligne de titre du tableau des comptes -->
        <thead>
            <tr>
                <th><input type='checkbox' id='cocheTout'/></th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Login</th>
                <th>Modifier</th>
                <th>Supprimer</th>                    
            </tr>
        </thead>
        <tbody>
            <!-- Ici on affiche les lignes, une par utilisateur -->
            <!-- cette variable montre comment on peut utiliser JSTL et EL pour calculer -->
        <c:set var="total" value="0"/>
        <form action="ServletUsers" method="post">
            <c:forEach var="u" items="${listeUsers}">
                <tr id='cases'>
                    <td><input type="checkbox" name="checkSupp" value="${u.id}"</td>
                    <td>${u.nom}</td>
                    <td>${u.prenom}</td>
                    <td>${u.login}</td>
                    <td><a href="ServletUsers?action=modifierUtilisateur&id=${u.id}"><img width="20px" src="ressources/crayon.png"/></a></td>
                    <td><a href="ServletUsers?action=supprimerUtilisateur&id=${u.id}"><img src="ressources/supprimer.png"/></a></td>
                    <!-- On compte le nombre de users -->
                <c:set var="total" value="${total+1}"/>
                </tr>
            </c:forEach>

            <!-- Affichage du solde total dans la derni�re ligne du tableau -->
            <tr>                       
                <td></td><td><b>TOTAL</b></td><td></td><td><b>${total}</b></td>
                <td></td>
                <td>                        
                    <input type="hidden" name="action" value="supprimerPlusieursUtilisateurs"/>
                    <input type="submit" class="btn btn-primary" value="Delete All Selected" name="submit"/>
                </td>
            </tr>
            </tbody>
    </table>
    <center><nav>
        <ul class="pagination">
        <c:if test="${requestScope['pageNumber'] gt 1}">
            <li>
            <a href="ServletUsers?action=precedent&index=${requestScope['pageNumber'] - 1}" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
            </a>
          </li>
        </c:if>

        <c:forEach begin="1" end="${requestScope['nbTotalPages'] }" var="i">
            <c:choose>
                <c:when test="${i!=pageNumber}">
                    <li><a class="nav" href="ServletUsers?action=getPage&index=${i}">${i} </a></li>
                </c:when>
                <c:otherwise>
                    <li class="active"><a class="nav" href="ServletUsers?action=getPage&index=${i}">${i} </a></li>
                </c:otherwise>        
            </c:choose>       
        </c:forEach>

        <c:if test="${requestScope['pageNumber'] lt requestScope['nbTotalPages'] }">
            <li>
                <a href="ServletUsers?action=suivant&index=${requestScope['pageNumber'] + 1}" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
        </c:if>
        </ul>
    </nav></center>
</c:if>
            
<c:if test="${param.action == 'modifierUtilisateur'}">
    <form action="ServletUsers" method="post">
        Login : <input type="text" class="form-control" name="login" value="${user.getLogin()}"/><br>
        Nom : <input type="text" class="form-control" name="nom" value="${user.getNom()}"/><br>
        Pr�nom : <input type="text" class="form-control" name="prenom" value="${user.getPrenom()}"/><br>
        Mail : <input type="text" class="form-control" name="email" value="${user.getEmail()}"/><br>
        <!--<input name="checkAdmin" type="checkbox" > Admin<br>-->
        Admin <input type="text" name="checkAdmin" list="exampleList" value="${user.isAdmin()}">
        <datalist id="exampleList">
            <option value="True">
            <option value="False">
        </datalist><br><br>
        <input type="hidden" name="action" value="updateUtilisateur"/>
        <input type="hidden" name="id" value="${user.getId()}"/>
        <input type="submit" class="btn btn-primary" value="Mettre � jour" name="submit"/>
    </form>
</c:if>
