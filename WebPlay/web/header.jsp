<%-- 
    Document   : header
    Created on : 29 avr. 2015, 09:56:52
    Author     : User
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!-- Ne pas oublier cette ligne sinon tous les tags de la JSTL seront ignorés ! -->
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <title>WebPlay</title>
        <link rel="icon" type="image/png" href="ressources/icon.png" />
        <meta charset="utf-8"></meta>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"></meta>
        <meta content="Bootstrap plugins licensed under the MIT license." name="description"></meta>
        <meta content="bootstrap, plugins, MIT, tag cloud, add, product images, download" name="author"></meta>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link type='text/css' href="ressources/bootstrap-tag-cloud.css" rel="stylesheet">
        <script src="ressources/bootstrap-tag-cloud.js"></script>
        <script>
            $('#myModal').on('shown.bs.modal', function () {
                $('#myInput').focus()
            })
        </script>        
        <script>
            $('#myTab a').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            })
        </script>
        <link rel="stylesheet" href="ressources/style.css" type='text/css'>
        <link rel="stylesheet" type="text/css" href="ressources/slideshow.css" media="screen" />
        <script type="text/javascript" src="ressources/jquery.min.js"></script>
        <script type="text/javascript" src="ressources/jquery.slideshow.lite.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(function () {
                $("#slideshow").slideshow();
                $("#slideshow2").slideshow({
                    pauseSeconds: 4,
                    height: 200,
                    caption: false
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#cocheTout').click(function () { // clic sur la case cocher/decocher

                    var cases = $("#cases").find(':checkbox'); // on cherche les checkbox qui dépendent de la liste 'cases'
                    if (this.checked) { // si 'cocheTout' est coché
                        cases.each(function () {
                            this.checked = true;
                        });
                    } else { // si on décoche 'cocheTout'
                        cases.each(function () {
                            this.checked = false;
                        });
                    }

                });

            });
        </script>
        <script>
            function Checkpwd2() {
                var pwd = document.getElementById('mdp').value;
                var pwd2 = document.getElementById('pwd2').value;
                if (pwd2.length != 0) {
                    if (pwd != pwd2) {
                        document.getElementById('pwd2_ch').src = 'ressources/false.gif';
                        document.getElementById('error_pwd2').innerHTML = 'Erreur: La confirmation ne correspond pas au mot de passe choisi';
                    }
                    else {
                        document.getElementById('pwd2_ch').src = 'ressources/check.gif';
                        document.getElementById('error_pwd2').innerHTML = '';
                    }
                }
            }
        </script>
    </head> 
    <body>

        <!-- Header -->
        <div class="header">
            <div class="col-md-4">
                <a class="navbar-brand" href="ServletArticles?action=listOnLoad">
                    <img alt="Brand" src="ressources/glyphicons-21-home.png">
                </a>
                <img id="logo" src="ressources/logo.png">
            </div>
            <div class="col-md-5">
                <form action="ServletSearch" method="post" class="navbar-form form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" id="search" name="titre" placeholder="Recherche ..." required>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="categories">
                            <option value="all">Catégorie...</option>
                            <option value="tuto">Tutoriels</option>
                            <option value="jeux">Jeux</option>
                            <option value="demo">Démos</option>
                        </select>
                    </div>
                    <input type="hidden" name="action" value="searchArticle"/>
                    <input type="submit" class="btn btn-success" value="Go !" name="submit"/>
                </form>
            </div>
            
            <form action="ServletLogin" method="post" name="form">
            <%  if (session.getAttribute("USER") == null && session.getAttribute("ADMIN") == null) { %>
            <div class="col-md-3">
                <button type="button" data-toggle="modal" data-target=".1" class="btn btn-default navbar-btn">Connectez-vous</button>
                <a href="inscription.jsp"><button type="button" class="btn btn-primary navbar-btn">Inscription</button></a>
            </div>
            <% } else { %>
            <div class="col-md-3">
                <% if(session.getAttribute("USER") != null){ %>                        
                Bienvenue <b> <%=session.getAttribute("USER")%></b> &nbsp;
                <% }else if(session.getAttribute("ADMIN") != null){ %>
                Bienvenue<b> <%=session.getAttribute("ADMIN")%></b> &nbsp;
                <% }%>
                <input type="hidden" name="action" value="logout"/>
                <input type="submit" class="btn btn-danger navbar-btn" value="Déconnexion" name="submit"/>	
                <c:out value="${sessionScope.user}"/>               
                <% if(session.getAttribute("ADMIN") != null){ %>  
                        &nbsp;<a href="admin.jsp"><img src="ressources/engrenage.png" /></a>
                <% } %>
            </div>                
            <% } %>
            </form>
        </div>

        <!-- Modal de connexion -->
        <div class="modal fade 1" id="modal-record" tabindex="-1" role="dialog" style="position:relative; padding-bottom:100%;" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h4 class="modal-title" id="myModalLabel">Connexion</h4></center>
                    </div>
                    <form action="ServletLogin" method="post" name="form">
                        <div class="modal-body">				 		
                            <h4>Pseudo</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="pseudo" id="pseudo"/>
                            </div>
                            <h4>Mot de passe</h4>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" id="password" />
                            </div>
                            <input type="checkbox" name="keep" value="oui" id="keep" checked="checked"> Se souvenir de moi <br /><br />
                            <a href="index.jsp">Mot de passe oublié ?</a>						
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="action" value="login"/>
                            <input type="submit" class="btn btn-primary" value="Login" name="submit"/>				        
                        </div>
                    </form>
                </div>
            </div>
        </div>