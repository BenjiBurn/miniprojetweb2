<div class="content">
    <%@include file="header.jsp" %>
</div>
<div class="container"  style="margin-bottom: 30px;">
    <div class="colg col-md-3">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            <a href="ServletUsers?action=listeUsers">Liste des utilisateurs</a>
                        </a>
                    </h4>
                </div>
                
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Recherche d'un utilisateur
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <form action="ServletUsers" method="get">
                            Login : <input class="form-control" type="text" name="login"/><br>
                            <input type="hidden" name="action" value="chercherParLogin"/>
                            <input class="btn btn-primary" type="submit" value="Chercher" name="submit"/>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <a href="ServletArticles?action=listeArticle">Liste des articles</a>
                        </a>
                    </h4>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFive">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            <a href="admin.jsp?action=newArticle">Ajouter un article</a>
                        </a>
                    </h4>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSix">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Recherche d'un article
                        </a>
                    </h4>
                </div>
                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                    <div class="panel-body">
                        <form action="ServletArticles" method="get">
                            Titre : <input class="form-control" type="text" name="titre"/><br>
                            <input type="hidden" name="action" value="chercherParTitre"/>
                            <input class="btn btn-primary" type="submit" value="Chercher" name="submit"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    <div class="cold col-md-9">
        <%@include file="contentadmin.jsp" %>
    </div>
</div>