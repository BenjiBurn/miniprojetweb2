<div class="content">
    <%@include file="header.jsp" %>
</div>
<div class="containerinscrp">
    <div class="panel panel-default">
        <div class="panel-heading" style="text-align: center;font-size: 20px;">Inscription</div>
        <div class="panel-body">
            <form action="ServletUsers" method="post">
                <div class="form-group">
                    <label for="pseudo">Pseudo</label>
                    <input type="text" class="form-control" id="pseudo" name="login" placeholder="Votre pseudo">
                </div>
                <div class="form-group">
                    <label for="mdp">Mot de passe</label>
                    <input type="password" class="form-control" id="mdp" name="mdp" placeholder="Mot de passe">
                </div>
                <div class="form-group">
                    <label for="pwd2">Mot de passe</label>
                <input class="form-control" type="password" name="pwd2" id="pwd2" onkeyup="Checkpwd2();" placeholder="Confirmer le mot de passe" onblur="Checkpwd2();" /><img src="./img/espace.gif" id="pwd2_ch" alt="" />
	<div id="error_pwd2" class="error"></div>
                </div>
                <div class="form-group">
                    <label for="name">Nom</label>
                    <input type="text" class="form-control" id="name" name="nom" placeholder="Votre nom">
                </div>
                <div class="form-group">
                    <label for="prenom">Pr�nom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Votre pr�nom">
                </div>
                <div class="form-group">
                    <label for="mail">Votre email</label>
                    <input type="email" class="form-control" id="mail" name="email" placeholder="yourmail@example.com">
                </div>
                <center>                    
                    <input type="hidden" name="action" value="newUser"/>
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                </center>
            </form>
        </div>