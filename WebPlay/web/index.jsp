<div class="content">
    <%@include file="header.jsp" %>
</div>
<c:if test="${empty param['action']}"> 
<% response.addHeader("Refresh","0.5; ServletOnLoad?action=listOnLoad"); %>
<div class="containerindex">
    <h2>Bienvenue sur WebPlay !</h2>
    <center><img width="150px" src="ressources/loading.gif" class="img-responsive" alt="Responsive image"></center>
</div>
</c:if>

<c:if test="${param['action'] == 'onload'}" >

<div class="containerarticle">
    <div class="col-md-4">
        <h3>Les mieux not�s</h3>
        <div id="slideshow2">            
            <c:forEach var="a" items="${listeMeilleurNote}">                
                <a href="ServletArticles?action=consulterArticle&id=${a.id}"><img src="${a.getFirstScreen()}" style="width:100%; margin-top:-30%;" alt="Lightning" /></a>                  
            </c:forEach>
        </div>
    </div>
    <div class="col-md-8">
        <h3>Derniers ajouts</h3>
        <div class="list-group" >
            <c:forEach var="a" items="${listeDerniersAjouts}">
                    <a href="ServletArticles?action=consulterArticle&id=${a.id}" class="list-group-item">${a.titre}<span class="badge">${a.categorie}</span></a>                    
            </c:forEach>
        </div>
    </div>
</div>
<div class="container">
    <h3>Derniers ajouts par cat�gories</h3>
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Jeux</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Tutoriels</a></li>
            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">D�mos</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="home">
                <div class="list-group">                                          
                    <c:forEach var="a" items="${listeJeux}">
                        <a href="ServletArticles?action=consulterArticle&id=${a.id}" class="list-group-item">${a.titre}<span class="badge">${a.date}</span></a>                    
                    </c:forEach>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="profile">
                <div class="list-group">                      
                    <c:forEach var="a" items="${listeTuto}">
                        <a href="ServletArticles?action=consulterArticle&id=${a.id}" class="list-group-item">${a.titre}<span class="badge">${a.date}</span></a>                    
                    </c:forEach>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="messages">
                <div class="list-group">                                   
                    <c:forEach var="a" items="${listeDemo}">
                        <a href="ServletArticles?action=consulterArticle&id=${a.id}" class="list-group-item">${a.titre}<span class="badge">${a.date}</span></a>                    
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</div>
</c:if>