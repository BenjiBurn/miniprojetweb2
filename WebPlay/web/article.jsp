<div class="content">
    <%@include file="header.jsp" %>
</div>

<div class="containerarticle">
    <div class="panel panel-default">
        <div class="panel-heading" style="text-align: center;font-size: 30px;" >${article.getTitre()}</div>
        <div class="panel-body">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-6">
                    Cat�gorie : 
                    <form action="ServletSearch" method="post" class="navbar-form form-inline">
                        <c:forEach var="a" items="${article.getCategorieSplit()}">
                            <input type="hidden" name="action" value="searchByCategorie"/>
                            <input type="hidden" name="categories" value="${a}"/>
                            <input type="submit" class="btn btn-primary btn-xs" value="${a}" name="submit"/>         
                        </c:forEach>
                    </form>
                </div>
                <div class="col-md-6">
                    Tags : 
                    <form action="ServletSearch" method="post" class="navbar-form form-inline">
                        <c:forEach var="tag" items="${article.getTagsSplit()}">
                            <input type="hidden" name="action" value="searchByTag"/>
                            <input type="hidden" name="tags" value="${tag}"/>
                            <input type="submit" class="btn btn-success btn-xs" value="${tag}" name="submit"/>         
                        </c:forEach>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div id="slideshow2">                          
                        <c:forEach var="lienScreen" items="${article.getScreenshotSplit()}">
                            <img src="${lienScreen}" class="img-responsive" style="width:100%; margin-top:-30%;"/>
                        </c:forEach>
                    </div>
                </div>
                <div class="col-md-8">
                    <p>${article.getDescription()}</p>
                    <c:set var="TotalNote" value="${a.totalNote}" /> 
                    <c:choose>
                        <c:when test="${article.totalNote >= '2'}" >
                            <img class="star" src="ressources/full.png"/>
                        </c:when>
                        <c:when test="${article.totalNote >= '1'}" >
                            <img class="star" src="ressources/demi.png"/>
                        </c:when>
                        <c:otherwise>
                            <img class="star" src="ressources/empty.png"/>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${article.totalNote >= '4'}" >
                            <img class="star" src="ressources/full.png"/>
                        </c:when>
                        <c:when test="${article.totalNote >= '3'}" >
                            <img class="star" src="ressources/demi.png"/>
                        </c:when>
                        <c:otherwise>
                            <img class="star" src="ressources/empty.png"/>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${article.totalNote >= '6'}" >
                            <img class="star" src="ressources/full.png"/>
                        </c:when>
                        <c:when test="${article.totalNote >= '5'}" >
                            <img class="star" src="ressources/demi.png"/>
                        </c:when>
                        <c:otherwise>
                            <img class="star" src="ressources/empty.png"/>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${article.totalNote >= '8'}" >
                            <img class="star" src="ressources/full.png"/>
                        </c:when>
                        <c:when test="${article.totalNote >= '7'}" >
                            <img class="star" src="ressources/demi.png"/>
                        </c:when>
                        <c:otherwise>
                            <img class="star" src="ressources/empty.png"/>
                        </c:otherwise>
                    </c:choose> 
                    <c:choose>
                        <c:when test="${article.totalNote >= '10'}" >
                            <img class="star" src="ressources/full.png"/>
                        </c:when>
                        <c:when test="${article.totalNote >= '9'}" >
                            <img class="star" src="ressources/demi.png"/>
                        </c:when>
                        <c:otherwise>
                            <img class="star" src="ressources/empty.png"/>
                        </c:otherwise>
                    </c:choose>
                    <br />
                    <p>
                    <div>
                        <form action="ServletArticles" method="get"  name="vote"><div class="descriptionNews">Noter :</div>
                            <select class="form-control" name="note" style="width:80px; float:left;">
                                <option value="0"> 0/10</option>
                                <option value="1"> 1/10</option>
                                <option value="2"> 2/10</option>
                                <option value="3"> 3/10</option>
                                <option value="4"> 4/10</option>
                                <option value="5" selected="selected"> 5/10</option>
                                <option value="6"> 6/10</option>
                                <option value="7"> 7/10</option>
                                <option value="8"> 8/10</option>
                                <option value="9"> 9/10</option>
                                <option value="10"> 10/10</option>
                            </select>&nbsp
                            <input type="hidden" name="action" value="noterArticle"/>
                            <input type="hidden" name="id" value="${article.getId()}"/>
                            <input type="submit" class="btn btn-primary" value="Envoyer" name="submit"/>
                        </form>
                    </div>                            
                    <p>R�dig� le : ${article.getDate()}</p>
                </div>
            </div>
            <div class="row" style="text-align: center;">
                <a href="${article.getLien()}"><button type="button" class="btn btn-primary btn-lg">Jouer !</button></a>                
            </div>
        </div>
    </div>
    <div class="panel-heading" style="text-align: center;font-size: 20px;">Commentaires </div>
    <div class="panel-body">
        <div class="containercom row">
            <c:forEach var="com" items="${listeCommentaire}">
                <div class="col-md-2 titrecom">${com.getUtilisateur()}</div>
                <div class="col-md-10">
                    ${com.getCommentaire()} <span class="badge" style="float:right;">${com.date}</span>
                </div>
                <div style="height:5px;"></div>
                <hr>
            </c:forEach>            
        </div>
         <% if(session.getAttribute("USER") != null || session.getAttribute("ADMIN") != null){ %>
            <div style="text-align: center;font-size: 20px;">Ajouter un commentaire</div><br>
            <form action="ServletCommentaire" method="post">            
                <textarea class="form-control" rows="3" name="commentaire" id="com"></textarea>
                <br />
                <input type="hidden" name="action" value="newCommentaire"/>
                <input type="hidden" name="article" value="${article.getId()}"/>            
                <% if(session.getAttribute("USER") != null){ %> 
                    <input type="hidden" name="utilisateur" value="<%=session.getAttribute("USER")%>"/>         
                <% } else if(session.getAttribute("ADMIN") != null){ %> 
                    <input type="hidden" name="utilisateur" value="<%=session.getAttribute("ADMIN")%>"/>   
                <% } %>
                <input class="btn btn-primary" name="submit" type="submit" value="Ajouter mon commentaire" />
            </form>  
        <% } %>
    </div>