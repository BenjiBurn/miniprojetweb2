<div class="content">
    <%@include file="header.jsp" %>
</div>

<div class="containerarticle">
    <div class="panel panel-default">
        <div class="panel-heading" style="text-align: center;font-size: 20px;">R�sultat de la recherche</div>
        <div class="panel-body"> 
            <c:if test="${param['action'] == 'empty'}" >
                <h3>Aucun r�sultat</h3>
            </c:if>
            <c:if test="${param['action'] == 'res'}" >
            <c:forEach var="a" items="${listeArticles}">
                <div class="result"><a href="ServletArticles?action=consulterArticle&id=${a.id}">
                        <div class="row">
                            <div class="col-md-3 "><img class="imgresult" src="${a.getFirstScreen()}" class="img-responsive"/>
                            </div>
                            <div class="col-md-9">
                                <p class="titreres">${a.titre}</p>
                                <p>${a.getDescription()}</p>
                                <c:set var="TotalNote" value="${a.totalNote}" /> 
                                <c:choose>
                                    <c:when test="${a.totalNote >= '2'}" >
                                        <img class="star" src="ressources/full.png"/>
                                    </c:when>
                                    <c:when test="${a.totalNote >= '1'}" >
                                        <img class="star" src="ressources/demi.png"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img class="star" src="ressources/empty.png"/>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${a.totalNote >= '4'}" >
                                        <img class="star" src="ressources/full.png"/>
                                    </c:when>
                                    <c:when test="${a.totalNote >= '3'}" >
                                        <img class="star" src="ressources/demi.png"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img class="star" src="ressources/empty.png"/>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${a.totalNote >= '6'}" >
                                        <img class="star" src="ressources/full.png"/>
                                    </c:when>
                                    <c:when test="${a.totalNote >= '5'}" >
                                        <img class="star" src="ressources/demi.png"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img class="star" src="ressources/empty.png"/>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${a.totalNote >= '8'}" >
                                        <img class="star" src="ressources/full.png"/>
                                    </c:when>
                                    <c:when test="${a.totalNote >= '7'}" >
                                        <img class="star" src="ressources/demi.png"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img class="star" src="ressources/empty.png"/>
                                    </c:otherwise>
                                </c:choose> 
                                <c:choose>
                                    <c:when test="${a.totalNote >= '10'}" >
                                        <img class="star" src="ressources/full.png"/>
                                    </c:when>
                                    <c:when test="${a.totalNote >= '9'}" >
                                        <img class="star" src="ressources/demi.png"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img class="star" src="ressources/empty.png"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        </a>
                    </div>
                    <hr>
                </c:forEach>
                </c:if>
        </div>
    </div>
</div>
<div>
<!--<center><nav>
            <ul class="pagination">
            <c:if test="${requestScope['pageNumber'] gt 1}">
                <li>
                <a href="ServletSearch?action=precedent&index=${requestScope['pageNumber'] - 1}" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
            </c:if>

            <c:forEach begin="1" end="${requestScope['nbTotalPages'] }" var="i">
                <c:choose>
                    <c:when test="${i!=pageNumber}">
                        <li><a class="nav" href="ServletSearch?action=getPage&index=${i}">${i} </a></li>
                    </c:when>
                    <c:otherwise>
                        <li class="active"><a class="nav" href="ServletSearch?action=getPage&index=${i}">${i} </a></li>
                    </c:otherwise>        
                </c:choose>       
            </c:forEach>

            <c:if test="${requestScope['pageNumber'] lt requestScope['nbTotalPages'] }">
                <li>
                    <a href="ServletSearch?action=suivant&index=${requestScope['pageNumber'] + 1}" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
            </c:if>
            </ul>
        </nav></center>-->
</div>